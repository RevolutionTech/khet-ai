import sys

from piece import InvalidMoveError, Piece, PieceSet, Pyramid, Scarab, Anubis, Pharaoh, Sphinx


class Board(object):

    NUM_COLS = 10
    NUM_ROWS = 8
    images = {
        'hor': u'\u2501',
        'vert': u'\u2503',
        'mid': u'\u254b',
        'ne': u'\u2513',
        'nw': u'\u250f',
        'sw': u'\u2517',
        'se': u'\u251b',
    }

    def __init__(self):
        # Initialize grid
        self.grid = []
        for col in xrange(0, self.NUM_COLS):
            this_col = []
            for row in xrange(0, self.NUM_ROWS):
                this_col.append(None)
            self.grid.append(this_col)

        # Create piece lists
        self.pieces = {
            'WHITE': PieceSet(),
            'RED': PieceSet(),
        }

        # Setup board
        self.setup_classic()

    def output(self, turn='NONE', laser=None):
        # Init
        i = 0
        reordered_pieces = []

        # Print out board
        sys.stdout.write('\n')
        for row in xrange(0, self.NUM_ROWS):
            # TOP
            for col in xrange(0, self.NUM_COLS):
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK'][laser.color])
                sys.stdout.write(self.images['nw'])
                sys.stdout.write(' ')
                sys.stdout.write(self.images['hor'])
                sys.stdout.write(' ')
                sys.stdout.write(self.images['ne'])
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK']['NONE'])
            sys.stdout.write('\n')

            # MIDDLE
            for col in xrange(0, self.NUM_COLS):
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK'][laser.color])
                sys.stdout.write(self.images['vert'])

                this_piece = self.grid[col][row]
                if this_piece is not None:
                    if this_piece.color == turn:
                        output_number = i
                        i += 1
                        reordered_pieces.append(this_piece.name)
                    else:
                        output_number = None
                    this_piece.output(number=output_number)
                else:
                    sys.stdout.write('   ')

                sys.stdout.write(self.images['vert'])
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK']['NONE'])
            sys.stdout.write('\n')

            # BOTTOM
            for col in xrange(0, self.NUM_COLS):
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK'][laser.color])
                sys.stdout.write(self.images['sw'])
                sys.stdout.write(' ')
                sys.stdout.write(self.images['hor'])
                sys.stdout.write(' ')
                sys.stdout.write(self.images['se'])
                if laser and laser.did_touch_point(col, row): sys.stdout.write(Piece.colors['BACK']['NONE'])
            sys.stdout.write('\n')

        # Update ordering of pieces
        if turn in ('WHITE', 'RED'):
            self.pieces[turn].piece_names = reordered_pieces

    def place_piece(self, piece, x, y):
        if x < 0 or y < 0 or x >= self.NUM_COLS or y >= self.NUM_ROWS:
            raise InvalidMoveError("You cannot move a piece off of the board.")
        piece_at_new_loc = self.grid[x][y]
        if isinstance(piece_at_new_loc, Scarab) or (not isinstance(piece, Scarab) and piece_at_new_loc):
            raise InvalidMoveError("This piece cannot be moved to this location as it is already occupied.")

        # Take the Scarab's place if it one entering this place
        if piece_at_new_loc:
            self.grid[piece.x][piece.y] = piece_at_new_loc
            piece_at_new_loc.place(piece.x, piece.y)
        # Or clear the space
        elif hasattr(piece, 'x'):
            self.grid[piece.x][piece.y] = None

        # Move the given piece to the given spot
        self.grid[x][y] = piece
        piece.place(x, y)

    def remove_piece(self, x, y):
        piece = self.grid[x][y]
        self.pieces[piece.color].remove_piece(piece.name)
        self.grid[x][y] = None
        return piece

    def setup_piece(self, piece, x, y):
        piece_name = self.pieces[piece.color].add_piece(piece)
        self.place_piece(piece, x, y)

    def setup_classic(self):
        # Place sphinxes
        self.setup_piece(piece=Sphinx(grid=self, color='RED', angle=270), x=0, y=0)
        self.setup_piece(piece=Sphinx(grid=self, color='WHITE', angle=90), x=self.NUM_COLS-1, y=self.NUM_ROWS-1)

        # Place pharaohs
        self.setup_piece(piece=Pharaoh(grid=self, color='RED', angle=270), x=5, y=0)
        self.setup_piece(piece=Pharaoh(grid=self, color='WHITE', angle=0), x=4, y=self.NUM_ROWS-1)

        # Place anubi
        self.setup_piece(piece=Anubis(grid=self, color='RED', angle=270), x=4, y=0)
        self.setup_piece(piece=Anubis(grid=self, color='RED', angle=270), x=6, y=0)
        self.setup_piece(piece=Anubis(grid=self, color='WHITE', angle=90), x=3, y=self.NUM_ROWS-1)
        self.setup_piece(piece=Anubis(grid=self, color='WHITE', angle=90), x=5, y=self.NUM_ROWS-1)

        # Place scarabs
        self.setup_piece(piece=Scarab(grid=self, color='RED', quadrant=2), x=4, y=3)
        self.setup_piece(piece=Scarab(grid=self, color='RED', quadrant=1), x=5, y=3)
        self.setup_piece(piece=Scarab(grid=self, color='WHITE', quadrant=3), x=4, y=4)
        self.setup_piece(piece=Scarab(grid=self, color='WHITE', quadrant=4), x=5, y=4)

        # Place pyramids
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=3), x=2, y=1)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=1), x=0, y=3)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=4), x=0, y=4)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=4), x=7, y=0)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=4), x=7, y=3)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=1), x=7, y=4)
        self.setup_piece(piece=Pyramid(grid=self, color='RED', quadrant=4), x=6, y=5)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=2), x=3, y=2)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=3), x=2, y=3)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=2), x=2, y=4)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=2), x=2, y=self.NUM_ROWS-1)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=1), x=7, y=6)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=2), x=self.NUM_COLS-1, y=3)
        self.setup_piece(piece=Pyramid(grid=self, color='WHITE', quadrant=3), x=self.NUM_COLS-1, y=4)
