import sys
import time

from ai import BasicHeuristicAI
from grid import Board
from laser import Laser
from piece import InvalidMoveError, LaserStopped, PieceCaptured
from player import Human


class GameOver(ValueError):
    pass


class InvalidChoice(ValueError):

    def __str__(self):
        return "Invalid choice: " + super(InvalidChoice, self).__str__()


class Controller(object):

    messages = {
        'player_turn': {
            'WHITE': "White player's turn.",
            'RED': "Red player's turn.",
        },
        'player_win': {
            'WHITE': "White player wins!",
            'RED': "Red player wins!",
        },
        'player_moves': {
            'WHITE': "White player moves a",
            'RED': "Red player moves a",
        },
        'player_rotates': {
            'WHITE': "White player rotates a",
            'RED': "Red player rotates a",
        },
        'player_lost_piece': {
            'WHITE': "White player lost a",
            'RED': "Red player lost a",
        },
    }

    def __init__(self):
        self.board = Board()
        self.players = {}

    @staticmethod
    def next_player(current_player):
        player = 'RED' if current_player == 'WHITE' else 'WHITE'
        return player

    @classmethod
    def print_moves_piece(self, piece, x, y):
        sys.stdout.write(
            "{player_moves} {piece_type} from ({piece_x}, {piece_y}) to ({piece_newx}, {piece_newy}).\n".format(
                player_moves=self.messages['player_moves'][piece.color],
                piece_type=piece.__class__.__name__,
                piece_x=piece.x,
                piece_y=piece.y,
                piece_newx=piece.x + x,
                piece_newy=piece.y + y,
            )
        )

    @classmethod
    def print_rotates_piece(self, piece, clockwise):
        sys.stdout.write(
            "{player_rotates} {piece_type} {direction}.\n".format(
                player_rotates=self.messages['player_rotates'][piece.color],
                piece_type=piece.__class__.__name__,
                direction='clockwise' if clockwise else 'counter-clockwise',
            )
        )

    @classmethod
    def print_lost_piece(self, piece):
        sys.stdout.write(
            "{player_lost_piece} {piece_type}.\n".format(
                player_lost_piece=self.messages['player_lost_piece'][piece.color],
                piece_type=piece.__class__.__name__
            )
        )

    def setup_game(self):
        # Choose number of human players
        while True:
            try:
                try:
                    num_humans = int(raw_input("How many human players? "))
                except ValueError:
                    raise InvalidChoice("0-2 human players must be selected.")
                if num_humans not in (0, 1, 2):
                    raise InvalidChoice("0-2 human players must be selected.")
            except InvalidChoice as e:
                print e
                continue
            break

        # Create humans and AI
        for i in xrange(0, 2):
            color = 'WHITE' if i == 0 else 'RED'
            if num_humans > 0:
                self.players[color] = Human(color=color)
                num_humans -= 1
            else:
                self.players[color] = BasicHeuristicAI(color=color)

        # Start game
        self.play_game()

    def fire_laser(self, turn):
        # Wait before firing
        print "Charging laser..."
        time.sleep(1)

        # Init laser
        sphinx_name = self.board.pieces[turn].pieces_by_type['Sphinx'][0]
        sphinx = self.board.pieces[turn].pieces_by_name[sphinx_name]
        laserx = sphinx.x; lasery = sphinx.y
        laser = Laser(color=turn, x=laserx, y=lasery, angle=sphinx.angle)

        # Move laser
        while True:
            # Move laser to next position
            if laser.angle == 0:
                laserx += 1
            elif laser.angle == 90:
                lasery -= 1
            elif laser.angle == 180:
                laserx -= 1
            elif laser.angle == 270:
                lasery += 1
            laser.add_point(laserx, lasery)

            # Display board
            self.board.output(turn='NONE', laser=laser)
            time.sleep(0.25)

            # Check for off board
            if laserx < 0 or laserx >= self.board.NUM_COLS or lasery < 0 or lasery >= self.board.NUM_ROWS:
                break

            # Check for collision
            collision = self.board.grid[laserx][lasery]
            if collision:
                try:
                    new_angle = collision.laser_collide(laser.angle)
                except PieceCaptured:
                    removed_piece = self.board.remove_piece(x=laserx, y=lasery)
                    self.print_lost_piece(removed_piece)

                    # Check for game end
                    if removed_piece.__class__.__name__ == 'Pharaoh':
                        self.end_game(loser=removed_piece.color)
                        raise GameOver()
                    break
                except LaserStopped:
                    break
                else:
                    laser.angle = new_angle

    def play_game(self):
        # White player starts
        turn = 'WHITE'

        while True:
            # Draw board
            self.board.output(turn=turn)
            print self.messages['player_turn'][turn]

            # Perform player move
            while True:
                move = self.players[turn].choose_move(controller=self, turn=turn)
                piece = self.board.pieces[turn].pieces_by_name[move.piece_name]
                action_string = move.action_string
                try:
                    if action_string == "cc":
                        self.print_rotates_piece(piece=piece, clockwise=False)
                        piece.rotate(clockwise=False)
                    elif action_string == "c":
                        self.print_rotates_piece(piece=piece, clockwise=True)
                        piece.rotate(clockwise=True)
                    elif 'u' in action_string or 'l' in action_string or 'd' in action_string or 'r' in action_string:
                        x = y = 0
                        if 'u' in action_string:
                            y -= 1
                        if 'l' in action_string:
                            x -= 1
                        if 'd' in action_string:
                            y += 1
                        if 'r' in action_string:
                            x += 1
                        self.print_moves_piece(piece=piece, x=x, y=y)
                        piece.move(x, y)
                except InvalidMoveError as im:
                    print InvalidChoice(str(im))
                    continue
                break

            # Draw board
            self.board.output(turn=turn)

            # Fire laser
            try:
                self.fire_laser(turn)
            except GameOver:
                break

            # Next player
            turn = self.next_player(turn)

    def end_game(self, loser):
        winner = 'RED' if loser == 'WHITE' else 'WHITE'
        print self.messages['player_win'][winner]


if __name__ == "__main__":
    Controller().setup_game()
