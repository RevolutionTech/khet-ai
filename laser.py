class Laser(object):

    def __init__(self, color, x, y, angle):
        self.color = color
        self.angle = angle
        self.points = {}
        self.add_point(x, y)

    def add_point(self, x, y):
        if x not in self.points:
            self.points[x] = {}
        self.points[x][y] = True

    def remove_point(self, x, y):
        self.points[x][y] = False

    def clear_points(self):
        self.points = {}

    def did_touch_point(self, x, y):
        try:
            return self.points[x][y]
        except KeyError:
            return False
