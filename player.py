import abc

from piece import Piece


class Move(object):

    def __init__(self, piece_name, action_string):
        self.piece_name = piece_name
        self.action_string = action_string


class Player(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self, color):
        self.color = color
        self.other_color = 'RED' if color == 'WHITE' else 'WHITE'

    @abc.abstractmethod
    def choose_move(self, controller, turn):
        pass


class Human(Player):

    def choose_move(self, controller, turn):
        # Perform human move
        while True:
            try:
                chosen_action = raw_input("Select a piece and action: ").lower()

                # Determine piece name
                try:
                    piece_selected = Piece.hex_to_int[chosen_action[0]]
                except IndexError:
                    continue # user types nothing, reprompt
                except KeyError:
                    raise InvalidChoice("Piece {piecenum} is not available on the board.".format(piecenum=chosen_action[0]))
                piece_name = controller.board.pieces[turn].piece_names[piece_selected]

                # Determine action
                action_string = chosen_action[1:]
                if not action_string:
                    raise InvalidChoice("An action was not provided.")
                elif action_string != "c" and action_string != "cc" and \
                    'u' not in action_string and 'l' not in action_string and \
                    'd' not in action_string and 'r' not in action_string:
                    raise InvalidChoice("Action {actionstr} does not describe a valid command.".format(actionstr=action_string))
            except InvalidChoice as e:
                print e
                continue
            else:
                break

        # Return move choice
        return Move(piece_name, action_string)
