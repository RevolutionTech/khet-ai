## Khet AI

#### Game owned by: Innovention Toys

#### AI created by: Lucas Connors

Notice: Khet is property of Innovention Toys. This product is not affiliated with Innovention Toys in any way. You can learn more about Khet from Innovention Toys' [website on Khet](http://khet.com/).
