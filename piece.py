import abc
import sys


class InvalidMoveError(ValueError):
    pass


class LaserStopped(ValueError):
    pass


class PieceCaptured(LaserStopped):
    pass


class Piece(object):

    __metaclass__ = abc.ABCMeta

    colors = {
        'NONE': '\x1b[39m',
        'WHITE': '\x1b[37m',
        'RED': '\x1b[31m',
        'BACK': {
            'NONE': '\x1b[49m',
            'WHITE': '\x1b[47m',
            'RED': '\x1b[41m',
        },
    }
    int_to_hex = {
        0: '0',
        1: '1',
        2: '2',
        3: '3',
        4: '4',
        5: '5',
        6: '6',
        7: '7',
        8: '8',
        9: '9',
        10: 'a',
        11: 'b',
        12: 'c',
    }
    hex_to_int = {v: k for k, v in int_to_hex.iteritems()}

    def __init__(self, grid, color):
        self.grid = grid
        self.color = color
        self.name = "-".join([self.__class__.__name__, self.color, unicode(id(self))])

    def output(self, number=None):
        sys.stdout.write(self.colors[self.color])
        if number is not None:
            sys.stdout.write(self.int_to_hex[number])
            sys.stdout.write(' ')
            self.output_image()
        else:
            sys.stdout.write(' ')
            self.output_image()
            sys.stdout.write(' ')
        sys.stdout.write(self.colors['NONE'])

    @abc.abstractmethod
    def output_image(self):
        pass

    def place(self, x, y):
        self.x = x
        self.y = y

    def move(self, x, y):
        # Check for valid movement
        if abs(x) > 1 or abs(y) > 1:
            raise InvalidMoveError("Pieces cannot move more than one space in any direction.")

        # Move the piece to (x, y)
        self.grid.place_piece(self, self.x + x, self.y + y)
        return self.x, self.y

    @abc.abstractmethod
    def rotate(self, clockwise=True):
        pass

    def laser_collide(self, laser_angle):
        raise PieceCaptured()


class Straight(Piece):

    def __init__(self, grid, color, angle):
        super(Straight, self).__init__(grid, color)
        self.angle = angle

    def output_image(self):
        sys.stdout.write(self.images[self.angle])

    def rotate(self, clockwise=True):
        if clockwise:
            self.angle = (self.angle + 270) % 360
        else:
            self.angle = (self.angle + 90) % 360


class Angle(Piece):

    def __init__(self, grid, color, quadrant):
        super(Angle, self).__init__(grid, color)
        self.quadrant = quadrant

    def output_image(self):
        sys.stdout.write(self.images[self.quadrant])

    def rotate(self, clockwise=True):
        if clockwise:
            self.quadrant = ((self.quadrant + 2) % 4) + 1
        else:
            self.quadrant = (self.quadrant % 4) + 1


class PieceSet(object):

    def __init__(self):
        self.pieces_by_name = {}
        self.pieces_by_type = {}
        self.piece_names = []

    def add_piece(self, piece):
        piece_type = piece.__class__.__name__
        self.pieces_by_name[piece.name] = piece
        if piece_type not in self.pieces_by_type:
            self.pieces_by_type[piece_type] = []
        self.pieces_by_type[piece_type].append(piece.name)
        self.piece_names.append(piece.name)

    def remove_piece(self, piece_name):
        piece = self.pieces_by_name[piece_name]
        del self.pieces_by_name[piece_name]
        self.pieces_by_type[piece.__class__.__name__].remove(piece_name)
        self.piece_names.remove(piece_name)


class Pyramid(Angle):

    images = {
        1: u'\u25e3',
        2: u'\u25e2',
        3: u'\u25e5',
        4: u'\u25e4',
    }

    def laser_collide(self, laser_angle):
        if self.quadrant == 1:
            if laser_angle == 180:
                return 90
            elif laser_angle == 270:
                return 0
            else:
                raise PieceCaptured()
        elif self.quadrant == 2:
            if laser_angle == 0:
                return 90
            elif laser_angle == 270:
                return 180
            else:
                raise PieceCaptured()
        elif self.quadrant == 3:
            if laser_angle == 0:
                return 270
            elif laser_angle == 90:
                return 180
            else:
                raise PieceCaptured()
        else:
            if laser_angle == 90:
                return 0
            elif laser_angle == 180:
                return 270
            else:
                raise PieceCaptured()


class Scarab(Angle):

    images = {
        1: u'\u2197',
        2: u'\u2196',
        3: u'\u2199',
        4: u'\u2198',
    }

    def laser_collide(self, laser_angle):
        if self.quadrant == 1 or self.quadrant == 3:
            if laser_angle == 0:
                return 90
            elif laser_angle == 90:
                return 0
            elif laser_angle == 180:
                return 270
            else:
                return 180
        else:
            if laser_angle == 0:
                return 270
            elif laser_angle == 90:
                return 180
            elif laser_angle == 180:
                return 90
            else:
                return 0


class Anubis(Straight):

    images = {
        0: u'\u2560',
        90: u'\u2569',
        180: u'\u2563',
        270: u'\u2566',
    }

    def laser_collide(self, laser_angle):
        if abs(self.angle - laser_angle) == 180:
            raise LaserStopped()
        else:
            raise PieceCaptured()


class Pharaoh(Straight):

    images = {
        0: u'\u265a',
        90: u'\u265a',
        180: u'\u265a',
        270: u'\u265a',
    }


class Sphinx(Straight):

    images = {
        0: u'\u21e2',
        90: u'\u21e1',
        180: u'\u21e0',
        270: u'\u21e3',
    }

    # The Sphinx cannot be moved
    def move(self, x, y):
        raise InvalidMoveError("The Sphinx cannot be moved.")

    # The Sphinx uses a special rotate function
    # because it has limited rotation capability
    def rotate(self, clockwise=True):
        if self.x == 0 and self.y == 0:
            if self.angle == 0:
                self.angle = 270
            else:
                self.angle = 0
        else:
            if self.angle == 90:
                self.angle = 180
            else:
                self.angle = 90

    # The Sphinx cannot be removed from the board
    def laser_collide(self, laser_angle):
        raise LaserStopped()
