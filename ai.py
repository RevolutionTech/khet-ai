import random

from player import Move, Player


class AI(Player):
    pass


class RandomAI(AI):

    def choose_move(self, controller, turn):
        piece_name = random.choice(controller.board.pieces[turn].piece_names)
        action_string = random.choice(["c", "cc", "u", "l", "d", "r", "ul", "ur", "dl", "dr"])
        return Move(piece_name, action_string)


class HeuristicAI(AI):

    piece_score = {
        'Pyramid': 2,
        'Scarab': 0, # cannot be lost
        'Anubis': 1,
        'Pharaoh': 15, # weight of Pharaoh must be > sum of all other pieces
        'Sphinx': 0, # cannot be lost
    }

    @classmethod
    def score_pieces(cls, pieces):
        return sum(map(lambda (piece_name, piecelst): cls.piece_score[piece_name] * len(piecelst), pieces.iteritems()))

    def score_board(self, board, complexity=0):
        # Incredibly simple score to calculate simply based
        # on the number of pieces on the board
        # "More of your pieces is bad, more of mine is good"
        if complexity == 0:
            my_pieces = board.pieces[self.color].piece_names
            your_pieces = board.pieces[self.other_color].piece_names
            return len(my_pieces) - len(your_pieces)
        # Slightly more complex scoring using different weights
        # for more valuable pieces
        elif complexity == 1:
            my_pieces = board.pieces[self.color].pieces_by_type
            your_pieces = board.pieces[self.other_color].pieces_by_type
            return self.score_pieces(my_pieces) - self.score_pieces(your_pieces)


class BasicHeuristicAI(HeuristicAI):

    def choose_move(self, controller, turn):
        print "The score of this board is:", self.score_board(controller.board, complexity=1)
        piece_name = random.choice(controller.board.pieces[turn].piece_names)
        action_string = random.choice(["c", "cc", "u", "l", "d", "r", "ul", "ur", "dl", "dr"])
        return Move(piece_name, action_string)
